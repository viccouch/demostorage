 // The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
 const functions = require('firebase-functions');

 // The Firebase Admin SDK to access the Firebase Realtime Database.
 const admin = require('firebase-admin');
 admin.initializeApp(functions.config().firebase);

 exports.helloWorld = functions.https.onRequest((request, response) => {
   response.send("Hola Ninja Hack!");

 exports.offersSubscribe = function subscribe(event, callback) {
   // The Cloud Pub/Sub Message object.
   const pubsubMessage = event.data;

   // We're just going to log the message to prove that
   // it worked.
   console.log(Buffer.from(pubsubMessage.data, 'base64').toString());

   // Push the new message into the Realtime Database using the Firebase Admin SDK.
   admin.database().ref('/offers').push({"offers": Buffer.from(pubsubMessage.data, 'base64').toString()}).then(snapshot => {
     console.log("se inserto en bd " + snapshot);
   });

   callback();
 };
